const Width = {
  Desktop: {
    S: 1024,
    M: 1200,
    L: 1366
  },
  Tablet: {
    L: 992,
    M: 768
  }
};

const Duration = {
  TAB: 400
};

const scrollHeight = Math.max(
  document.body.scrollHeight, document.documentElement.scrollHeight,
  document.body.offsetHeight, document.documentElement.offsetHeight,
  document.body.clientHeight, document.documentElement.clientHeight
);

const docWidth = $(document).outerWidth();
const wrapperWidth = $('.offers__wrapper').outerWidth();
const freeSpaceHalf = (docWidth - wrapperWidth) / 2;

export {
  Width,
  Duration,
  scrollHeight,
  freeSpaceHalf
};
