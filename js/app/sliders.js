import Swiper from 'swiper';

import {
  Width,
  freeSpaceHalf
} from './../util';

$(document).ready(() => {
  const fullpageSwiper = new Swiper('.fullpage', {
    direction: 'vertical',
    height: document.documentElement.clientHeight,
    speed: 700,
    mousewheel: true,
    simulateTouch: false,
    on: {
      init () {
        $('body').css('overflow', 'hidden');
      }
    }
  });

  $('#map').hover(fullpageSwiper.mousewheel.disable, fullpageSwiper.mousewheel.enable);

  $('#contacts').click((evt) => {
    evt.preventDefault();
    fullpageSwiper.slideTo(fullpageSwiper.slides.length - 1);
  });

  if (window.screen.width < Width.Desktop.M) {
    fullpageSwiper.destroy(false, true);
    $('body').css('overflow', '');
  }

  const $gallery = $('.gallery');

  $gallery.css('right', -freeSpaceHalf);

  new Swiper('.gallery', {
    speed: 700,
    autoplay: {
      delay: 5000
    },
    pagination: {
      el: '.gallery__pagination',
      clickable: true,
      loop: true,
      bulletClass: 'gallery__bullet',
      bulletActiveClass: 'gallery__bullet--active'
    },
    on: {
      init () {
        const instance = this;
        const $counter = $(instance.$el).parent().find('.counter');

        $.when($(instance.slides).each((i, slide) => {
          $counter.find('.counter__list')
              .append(`<li class="counter__item swiper-slide">0${++i}</li>`);
        })).then(() => {
          const counterSwiper = new Swiper('.counter', {
            slidesPerView: 3,
            direction: 'vertical',
            spaceBetween: 30,
            centeredSlides: true,
            simulateTouch: false,
            slideActiveClass: 'counter__item--active',
            slideNextClass: 'counter__item--next',
            slidePrevClass: 'counter__item--prev'
          });

          instance.controller.control = counterSwiper;
        });
      }
    }
  });

  const galleryHeight = $('.offers__gallery').outerHeight();

  if (window.screen.width >= Width.Tablet.L) {
    $('.offers__slider').css('height', galleryHeight);
  }
});
