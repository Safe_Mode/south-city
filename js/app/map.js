$(document).ready(() => {
  const coords = [];
  const $locations = $('.contacts__item');

  $locations.each((i, loc) => {
    const locCoordsStr = $(loc).data('coords');

    if (locCoordsStr) {
      const locCoords = locCoordsStr.split(', ');
      coords.push(locCoords);
    }
  });

  const setAddressToBalloon = function (placemark) {
    return ymaps.geocode(placemark.geometry._coordinates).then(function (res) {
      const firstGeoObject = res.geoObjects.get(0);

      placemark.properties.set({
        balloonContent: firstGeoObject.getAddressLine()
      });

      return firstGeoObject.getAddressLine();
    });
  };

  const getMapCenter = function (map) {
    const mapCenter = [];

    map.getCenter().forEach(function (coord) {
      mapCenter.push(coord);
    });

    return mapCenter;
  };

  // const setPassivePlacemarkState = function (array) {
  //   array.each(function (placemark) {
  //     placemark.options.set({
  //       iconImageHref: SVG_ENDPOINT + 'map-marker.svg'
  //     });
  //   });
  // };

  // const setActivePlacemarkState = function (placemark, map) {
  //   const contactMapCenter = getMapCenter(map);
  //
  //   if (placemark.geometry._coordinates.every(function (coord, i) {
  //     return Math.round(coord * ROUND_ACCURACY) / ROUND_ACCURACY === contactMapCenter[i];
  //   })) {
  //     placemark.options.set({
  //       iconImageHref: SVG_ENDPOINT + 'map-marker-active.svg'
  //     });
  //   }
  // };

  const $contactsWrpr = $('.contacts__wrapper');
  const freeSpaceHalf = (document.documentElement.clientWidth - $contactsWrpr.outerWidth()) / 2;

  if ($contactsWrpr.outerHeight() > $(window).height()) {
    $('.contacts__list').accordion({
      header: '.location__title'
    });
  }

  $('.map__container').css('right', -freeSpaceHalf);

  ymaps.ready(function () {
    if ( $('#map').length ) {
      const contactMap = new ymaps.Map('map', {
        center: coords[0],
        zoom: 17,
        controls: []
      });

      const contactMarks = new ymaps.GeoObjectCollection({}, {
        preset: 'islands#darkGreenDotIcon'
        // iconLayout: 'default#image',
        // iconImageHref: SVG_ENDPOINT + 'map-marker.svg',
        // iconImageSize: [50, 55]
      });

      coords.forEach((mark) => {
        contactMarks.add(new ymaps.Placemark(mark));
      });

      contactMap.geoObjects.add(contactMarks);

      const activeClass = 'location__link--active';
      let $links = $('.location[data-coords] .location__link');

      $links.first().addClass(activeClass);

      contactMarks.each(function (placemark) {
        $.when(setAddressToBalloon(placemark)).done(function (res) {
          $($links[contactMarks.indexOf(placemark)]).data('address', res);
        });

       // setActivePlacemarkState(placemark, contactMap);
      });

      $('body').on('click', '.location__link', function (evt) {
        evt.preventDefault();

        contactMarks.each(function (placemark) {
          if ($(evt.currentTarget).data().address === placemark.properties.get('balloonContent')) {
            $links.each(function (i, it) {
              const index = placemark
                  .getParent()
                  .indexOf(placemark);

              if (i !== index) {
                $(it).removeClass(activeClass);
              } else {
                if (!$(it).hasClass(activeClass)) {
                  $(it).addClass(activeClass)
                }
              }
            });

            const newCenter = [];

            placemark.geometry._coordinates.forEach((coord) => {
              newCenter.push(parseFloat(coord));
            });

            contactMap
                .panTo(newCenter)
                .then(function () {
              // $.when( setPassivePlacemarkState(contactMarks) ).done(function () {
              //   setActivePlacemarkState(placemark, contactMap)
              // });
            });
          }
        });
      });
    }
  });
});
