import 'bootstrap';
// import objectFitImages from 'object-fit-images';
import '../node_modules/@fancyapps/fancybox/dist/jquery.fancybox';

import './app/sliders';
import './app/map';
import {
  Width,
  Duration,
  freeSpaceHalf
} from './util';

const DEC_SCALE = 10;
const dotSettings = {
  truncate: 'letter'
};

$(document).ready(() => {
  // objectFitImages('.services__image');

  $('.dottable').dotdotdot(dotSettings);

  $('.stock__list p').dotdotdot();
  $('.stock__list h3').dotdotdot(dotSettings);
  $('.different__list h3').dotdotdot();
  $('.sponsors__card ul').dotdotdot();
  $('.sponsors__card p').dotdotdot();
  $('.features__list li').dotdotdot(dotSettings);
  $('.news__list li').dotdotdot(dotSettings);
  $('.promo__services li').dotdotdot(dotSettings);

  $('.different__list li').each((i, it) => {
    const $link = $(it).find('a:last-child');

    if ($link.length) {
      $(it).dotdotdot({
        truncate: 'letter',
        callback(isTruncated) {
          if (isTruncated) {
            $(this).append($link);
          }
        }
      })
    }
  });

  const wrapperPLeft = parseInt( $('.sponsors__wrapper').css('padding-left'), DEC_SCALE );

  if (window.screen.width >= Width.Tablet.M) {
    $('.sponsors__image').css('margin-left', -(freeSpaceHalf + wrapperPLeft));
  }

  $('.news__content').tabs({
    show: Duration.TAB,
    hide: Duration.TAB,
    active: 0,
    classes: {
      'ui-tabs-tab': 'news__tab',
      'ui-tabs-active': 'news__tab--active'
    },
    create (evt, ui) {
      $('[role=tabpanel]').each((i, it) => {
        $(it).append(`<span class="news__count">0${i + 1}</span>`);
      });
    }
  });
});
